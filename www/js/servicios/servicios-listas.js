    angular.module('starter')
        .service('listasService', function($q, $http){
        var results;
        var supermercados;
        
        return {
            doEstado        : doEstado,
            getEstadoListas : getEstadoListas,
            doSupermercados : doSupermercados,
            getSupermercados : getSupermercados,
        };

        function doEstado(id_lista_super , name_bd) {
            var deferred = $q.defer();
            //var url = '/admin/productos/'+ texto + '/' + name_bd;
            var url = urlws + "/lista/estados/" + name_bd + "/" + id_lista_super;
            $http.get(url)
              .then(function(res) {
                var resul = res.data;
                results = resul.data[0];
                deferred.resolve(results);
                }, function(err){
                    deferred.resolve(err);
                });

            return deferred.promise;
        };

        function doSupermercados() {
            var deferred = $q.defer();
            //var url = '/admin/productos/'+ texto + '/' + name_bd;
            var url = urlws + "/supermercados";
            $http.get(url)
              .then(function(res) {
                var resul = res.data;
                //supermercados = resul.data[0];
                supermercados = resul;
                deferred.resolve(supermercados);
                }, function(err){
                    deferred.resolve(err);
                });

            return deferred.promise;
        };

        function getEstadoListas(){
            return results;
        }
        function getSupermercados(){
            console.log("retornar los supermercados");
            return supermercados;
        }        

});
