var http ;
var datalista = [];
var bd_super;
var lista;
var $cordovaSQLite;
var $translate;

var shopPaypal = {
  // shopPaypallication Constructor
  initialize: function() {
    this.bindEvents();
  },
  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicity call 'shopPaypal.receivedEvent(...);'
  onDeviceReady: function() {
    shopPaypal.receivedEvent('deviceready');
  },
  // Update DOM on a Received Event
  receivedEvent: function(id) {
    // start to initialize PayPalMobile library
    shopPaypal.initPaymentUI();
  },
  initPaymentUI: function() {
    var clientIDs = {
      "PayPalEnvironmentProduction": "YOUR_PRODUCTION_CLIENT_ID",
      "PayPalEnvironmentSandbox": id_paypal
    };
    PayPalMobile.init(clientIDs, shopPaypal.onPayPalMobileInit);

  },
  onSuccesfulPayment: function(payment) {
    console.log("payment success: " + JSON.stringify(payment, null, 4));
    console.log(">>: response" + payment.response.state);
      if (payment.response.state === 'approved') {
          
          shopPaypal.enviarListaSupermercado();
          shopPaypal.mensaje();
      }
  },
  mensaje : function(){
    $translate(['pagadacorrecta']).then(function (translations) {
      window.plugins.toast.showShortBottom(translations.pagadacorrecta + ". !", function (a) {
          console.log('toast success: ' + a)
      }, function (b) {
          alert('toast error: ' + b)
      });
    })
  },
  enviarListaSupermercado : function(){
      console.log("es hora de enviar al super");

        var url = urlws + "/listas/" + bd_super;
        http.post(url, datalista).success(function (respuesta) {

          console.log("respuesta server >> " , respuesta.data);

          //activar el boton para el seguimiento
          //guardar el id_lista bd en mis listas
          //$ionicPopup (lista pagada satisfactoriamente)

          shopPaypal.actualizarLista(respuesta.data);


        }).error(function (data, status, headers, config) {
            var token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
            console.log('-->' + token);
        });
  },

  actualizarLista : function(id_lista_super){
    //actualizar todo agrege un nuevo campo en la bd movil (borrar bd)
    //TODO : debemos guardar el id de la lista del supermercado en mi movil
      var update = "UPDATE listasrk SET id_lista_super = ? WHERE ID = ?";
      $cordovaSQLite.execute(db, update, [id_lista_super, lista.id]).then(function (res) {
        lista.id_lista_super = id_lista_super;
        console.log("filas >> " , res);
      }, function (err) {
          console.error(err);
      });
  },

  onAuthorizationCallback: function(authorization) {
    console.log("authorization: " + JSON.stringify(authorization, null, 4));
  },
  createPayment: function(dolares , lista) {
    // for simplicity use predefined amount
    var paymentDetails = new PayPalPaymentDetails(dolares, "0.00", "0.00");
    /*var payment = new PayPalPayment("50.00", "USD", "Awesome Sauce", "Sale",
      paymentDetails);*/
    var payment = new PayPalPayment(dolares, "USD", lista.nombre, "Sale",
      paymentDetails);
    return payment;
  },
  configuration: function() {
    // for more options see `paypal-mobile-js-helper.js`
    var config = new PayPalConfiguration({
      merchantName: "Shop-List",
      merchantPrivacyPolicyURL: "https://mytestshop.com/policy",
      merchantUserAgreementURL: "https://mytestshop.com/agreement",
      languageOrLocale: lenguaje
    });
    return config;
  },

  comprar: function(dolares, _lista, data, $http, _bd_super, _$cordovaSQLite, _$translate) {
    //load data of lista to send server
    datalista = data;
    http      = $http;
    bd_super  = _bd_super;
    lista     = _lista;
    $cordovaSQLite = _$cordovaSQLite;
    $translate = _$translate;
  // single payment
  PayPalMobile.renderSinglePaymentUI(
    shopPaypal.createPayment(dolares , lista), 
    shopPaypal.onSuccesfulPayment,
    shopPaypal.onUserCanceled);
  },

  onPrepareRender: function() {
    // buttons defined in index.html
    //  <button id="buyNowBtn"> Buy Now !</button>
    //  <button id="buyInFutureBtn"> Pay in Future !</button>
    //  <button id="profileSharingBtn"> ProfileSharing !</button>
    var buyNowBtn = document.getElementById("buyNowBtn");
    var buyInFutureBtn = document.getElementById("buyInFutureBtn");
    var profileSharingBtn = document.getElementById("profileSharingBtn");
  },
  onPayPalMobileInit: function() {
    // must be called
    // use PayPalEnvironmentNoNetwork mode to get look and feel of the flow
    /*PayPalMobile.prepareToRender("PayPalEnvironmentNoNetwork", shopPaypal.configuration(),
      shopPaypal.onPrepareRender);*/
    PayPalMobile.prepareToRender("PayPalEnvironmentSandbox", shopPaypal.configuration(),
      shopPaypal.onPrepareRender);
  },
  onUserCanceled: function(result) {
    console.log(result);
  }
};


