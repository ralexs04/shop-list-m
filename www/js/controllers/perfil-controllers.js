//angular.module('angular.module('my.controllers', [])')
angular.module('starter')
.controller('PerfilCtrl', function ($scope, 
                                    $http, 
                                    $state,
                                    StorageService,
                                    $translate) {

    $scope.cargarDatosUser      = cargarDatosUser;
    $scope.showPerfil           = showPerfil;
    $scope.actualizarPerfil     = actualizarPerfil;
    $scope.mostrarActualizar    = mostrarActualizar;

    function cargarDatosUser(){
        var arreglo = StorageService.getUser();
        usr = arreglo[0];
        $scope.usuario = usr[0];
        cargarDatosEdit(usr[0]);
    }

    function cargarDatosEdit(usuario){
        $scope.usrEdit = {id : usuario.id};
        document.getElementById('nombres').value    = usuario.nombres;
        document.getElementById('apellidos').value  = usuario.apellidos;
        document.getElementById('correo').value     = usuario.email;
    }

    $scope.mostrarPerfil = true;
    $scope.editarPerfil = false;

    function showPerfil(){
        $scope.mostrarPerfil = $scope.mostrarPerfil === true ? false : true;
        $scope.editarPerfil  = $scope.editarPerfil  === false ? true : false;
        cargarDatosUser();
    }
    
    function actualizarPerfil(usuario){

        if (usuario.password === ""){
            //usuario.password = $scope.usuario.password;
            delete usuario.password;
        }
        if (usuario.email === ""){
            //remover este elemento del elemento a ennviar
            delete usuario.email;
        }
    
        var url = urlws + "/cliente/" + usuario.id ;
        $http.put(url , usuario).success(function (respuesta) {
            //respuesta del servidor
            $scope.mostrarPerfil = true;
            $scope.editarPerfil = false;

            $translate(['actualizarperfil']).then(function (translations) {
                window.plugins.toast.showShortBottom(translations.actualizarperfil + ". !", function (a) {
                    console.log('toast success: ' + a)
                }, function (b) {
                    alert('toast error: ' + b)
                });
            });


        }).error(function (data, status, headers, config) {
            $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
            console.log('-->' + $scope.token);
            $scope.mostrarPerfil = true;
            $scope.editarPerfil = false;

            $translate(['actualizarperfilerror']).then(function (translations) {
                window.plugins.toast.showShortBottom(translations.actualizarperfilerror + ". !", function (a) {
                    console.log('toast success: ' + a)
                }, function (b) {
                    alert('toast error: ' + b)
                });
            });
        });
    }
    
    $scope.showActualizar = false;
    function mostrarActualizar (){
        $scope.showActualizar = $scope.showActualizar === true ? false : true;   
    }

});
