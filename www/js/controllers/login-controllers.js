
angular.module('loginController', [])

.controller('loginCtrl', function ($scope, 
                                   $http, 
                                   $state, 
                                   $location, 
                                   $ionicHistory, 
                                   StorageService,
                                   $window,
                                   $ionicLoading,
                                   $ionicPopup) {
    
	$scope.login = function (user) {
    //activar loading
    showLoading($ionicLoading);

		$ionicHistory.nextViewOptions({
    		disableBack: true
  	});

    var url = urlws + "/loginCliente";
    $http.post(url, user).success(function (respuesta) {
      //ocultar loading
      hideLoaging($ionicLoading);
      //respuesta del servidor
      if (respuesta.msg === 'Ok') {
        //guardar usuario en localstorage
        StorageService.deleteUSer();
        StorageService.saveUser(respuesta.data);
        $state.go('supermercados');
      } else {
          console.log("error al logear");
          showAlertLogin($ionicPopup);
      }
    }).error(function (data, status, headers, config) {
      $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
      console.log('-->' + $scope.token);
      hideLoaging($ionicLoading);
    });
  }
  $scope.logout = function() {
    $window.localStorage.clear();
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
    StorageService.deleteUSer();
    $state.go('login');
  };
})

.controller('registrarseCtrl', function ($scope,
                                         $http,
                                         $state,
                                         $ionicLoading,
                                         $ionicHistory) {

  $ionicHistory.nextViewOptions({
    disableBack: false
  });

	$scope.usuario_registrado = function (user) {
    showLoading($ionicLoading);
    var url = urlws + "/cliente";
    $http.post(url , user).success(function (respuesta) {
      //respuesta del servidor
      hideLoaging($ionicLoading);
      $state.go('login');

    }).error(function (data, status, headers, config) {
        $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
        console.log('-->' + $scope.token);
        hideLoaging($ionicLoading);
    });
  }
});

showAlertLogin = function($ionicPopup) {
  var alertPopup = $ionicPopup.alert({
    title: 'Datos Incorrectos',
    template: 'Los datos ingresados son incorrectos. Porfavor intente denuevo'
  });
  alertPopup.then(function(res) {
    console.log('Thank you for not eating my delicious ice cream cone');
  });
};

//metodos de loading
showLoading = function($ionicLoading) {
  $ionicLoading.show({
    content: 'Loading',
    animation: 'fade-in',
    showBackdrop: true,
    maxWidth: 200,
    showDelay: 0
  });
};

hideLoaging = function ($ionicLoading){
  $ionicLoading.hide();
};